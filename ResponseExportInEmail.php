<?php

/**
 * Plugin for LimeSurvey
 * Attach export of response in confirmation and admin email.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu <https://sondages.pro>
 * @copyright 2022 OECD <https://oecd.org>
 * @license AGPL v3
 * @version 0.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class ResponseExportInEmail extends PluginBase
{

    protected static $name = 'ResponseExportEmail';
    protected static $description = 'Attach export of response in confirmation and admin email.';

    protected $storage = 'DbStorage';

    public $allowedPublicMethods = [];

    /** @var integer $responseId keep it between event*/
    private $responseId = 0;
    /** @var string[] $tempFiles to be deleted when view */
    private $tempFiles = [];

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        /* setting */
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');

        /* attach */
        $this->subscribe('beforeSurveyEmail', 'attachExport');
        $this->subscribe('beforeTokenEmail', 'attachExport');

        /* set the config */
        $ResponseExportInEmailByConfig = App()->getConfig('ResponseExportInEmail');
        App()->setConfig(
            'ResponseExportInEmail',
            include(__DIR__ . "/config-default.php")
        );
        if (is_array($ResponseExportInEmailByConfig)) {
            App()->setConfig(
                'ResponseExportInEmail',
                array_replace_recursive(
                    App()->getConfig('ResponseExportInEmail'),
                    $ResponseExportInEmailByConfig
                )
            );
        }
    }

    /**
     * Register the settings
     */
    public function beforeSurveySettings()
    {
        $oEvent = $this->event;
        $surveyId = $oEvent->get('survey');
        $exportList = $this->getAvailableExportList();
        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
                'settings' => array(
                    'attachOn_confirmation' => array(
                        'type' => 'select',
                        'label' => $this->translate('Attach on confirmation email'),
                        'options' => $exportList,
                        'htmlOptions' => array(
                            'empty' => gT("None"),
                        ),
                        'current' => $this->get('attachOn_confirmation', 'Survey', $surveyId, ''),
                    ),
                    'attachOn_confirmation_filename' => array(
                        'type' => 'string',
                        'label' => $this->translate('Filename of attached file'),
                        'htmlOptions' => array(
                            'placeholder' => 'response_{SID}_{SAVEDID}',
                        ),
                        'current' => $this->get('attachOn_filename', 'Survey', $surveyId, ''),
                    ),
                    'attachOn_confirmation_headingFormat' => array(
                        'type' => 'select',
                        'label' => $this->translate('Export questions text as'),
                        'options' => array(
                            'code' => gT("Question code"),
                            'abbreviated' => gT("Abbreviated question text"),
                            'full' => gT("Full question text"),
                            'codetext' => gT("Question code & question text"),
                        ),
                        'current' => $this->get('attachOn_confirmation_headingFormat', 'Survey', $surveyId, 'full'),
                    ),
                    'attachOn_confirmation_answerFormat' => array(
                        'type' => 'select',
                        'label' => $this->translate('Export responses as'),
                        'options' => array(
                            'short' => gT("Answer codes"),
                            'long' => gT("Full answers"),
                        ),
                        'current' => $this->get('attachOn_confirmation_answerFormat', 'Survey', $surveyId, 'long'),
                    ),
                    'attachOn_admin_notification' => array(
                        'type' => 'select',
                        'label' => $this->translate('Attach on basic admin notification'),
                        'options' => $exportList,
                        'htmlOptions' => array(
                            'empty' => gT("None"),
                        ),
                        'current' => $this->get('attachOn_admin_notification', 'Survey', $surveyId, ''),
                    ),
                    'attachOn_admin_notification_filename' => array(
                        'type' => 'string',
                        'label' => $this->translate('Filename of attached file'),
                        'htmlOptions' => array(
                            'placeholder' => 'response_{SID}_{SAVEDID}',
                        ),
                        'current' => $this->get('attachOn_admin_notification_filename', 'Survey', $surveyId, ''),
                    ),
                    'attachOn_admin_notification_headingFormat' => array(
                        'type' => 'select',
                        'label' => $this->translate('Export questions text as'),
                        'options' => array(
                            'code' => gT("Question code"),
                            'abbreviated' => gT("Abbreviated question text"),
                            'full' => gT("Full question text"),
                            'codetext' => gT("Question code & question text"),
                        ),
                        'current' => $this->get('attachOn_admin_notification_headingFormat', 'Survey', $surveyId, 'full'),
                    ),
                    'attachOn_admin_notification_answerFormat' => array(
                        'type' => 'select',
                        'label' => $this->translate('Export responses as'),
                        'options' => array(
                            'short' => gT("Answer codes"),
                            'long' => gT("Full answers"),
                        ),
                        'current' => $this->get('attachOn_admin_notification_answerFormat', 'Survey', $surveyId, 'long'),
                    ),
                    'attachOn_admin_responses' => array(
                        'type' => 'select',
                        'label' => $this->translate('Attach on detailed admin notification'),
                        'options' => $exportList,
                        'htmlOptions' => array(
                            'empty' => gT("None"),
                        ),
                        'current' => $this->get('attachOn_admin_responses', 'Survey', $surveyId, ''),
                    ),
                    'attachOn_admin_responses_filename' => array(
                        'type' => 'string',
                        'label' => $this->translate('Filename of attached file'),
                        'htmlOptions' => array(
                            'placeholder' => 'response_{SID}_{SAVEDID}',
                        ),
                        'current' => $this->get('attachOn_admin_responses_filename', 'Survey', $surveyId, ''),
                    ),
                    'attachOn_admin_responses_headingFormat' => array(
                        'type' => 'select',
                        'label' => $this->translate('Export questions text as'),
                        'options' => array(
                            'code' => gT("Question code"),
                            'abbreviated' => gT("Abbreviated question text"),
                            'full' => gT("Full question text"),
                            'codetext' => gT("Question code & question text"),
                        ),
                        'current' => $this->get('attachOn_admin_responses_headingFormat', 'Survey', $surveyId, 'full'),
                    ),
                    'attachOn_admin_responses_answerFormat' => array(
                        'type' => 'select',
                        'label' => $this->translate('Export responses as'),
                        'options' => array(
                            'short' => gT("Answer codes"),
                            'long' => gT("Full answers"),
                        ),
                        'current' => $this->get('attachOn_admin_responses_answerFormat', 'Survey', $surveyId, 'long'),
                    ),
                ),
        ));
    }

    /** @inheritdoc **/
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    /** delete tmp file after sent */
    public function deleteTempFiles()
    {
        foreach ($this->tempFiles as $file) {
            tracevar($file);
            @unlink($file);
        }
    }
    /** check if need attach export and attach */
    public function attachExport()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $type = $event->get('type');
        $surveyId = $event->get('survey');
        $srid = null;
        if (empty($_SESSION['survey_' . $surveyId]['srid'])) {
            return;
        }
        $srid = $_SESSION['survey_' . $surveyId]['srid'];
        $exportType = $this->get('attachOn_' . $type, 'Survey', $surveyId, '');
        if (empty($exportType)) {
            return;
        }
        $config = App()->getConfig('ResponseExportInEmail');
        $mailer = $event->get('mailer');
        //$this->subscribe('afterSurveyComplete', 'deleteTempFiles');
        $exportFile = $this->getExportFile(
            $surveyId,
            $srid,
            $exportType,
            $this->get('attachOn_' . $type . '_headingFormat', 'Survey', $surveyId, 'full'),
            $this->get('attachOn_' . $type . '_answerFormat', 'Survey', $surveyId, 'long')
        );
        if ($exportFile) {
            if (!is_file($exportFile)) {
                $this->log("File generated for survey {$surveyId} , response {$srid} on {$type} not found", 'error');
            }
            $filename = "response_{$surveyId}_{$srid}";
            if ($this->get('attachOn_' . $type . '_filename', 'Survey', $surveyId, '')) {
                $filename = $this->get('attachOn_' . $type . '_filename', 'Survey', $surveyId, '');
                $aReplacements = array( 'SID' => $surveyId, 'SAVEDID' => $srid );
                $filename = LimeExpressionManager::ProcessStepString($filename, $aReplacements, 3, true);
            }
            $extensionByTypeByConfig = $config['extensionByType'];
            if (isset($extensionByTypeByConfig[$exportType]) && is_string($extensionByTypeByConfig[$exportType])) {
                $extension = $extensionByTypeByConfig[$exportType];
            } else {
                $extension = LSFileHelper::getExtensionByMimeType($exportFile);
            }
            if (empty($extension)) {
                $extension = $exportType;
            }
            $mailer->addAttachment($exportFile, $filename . '.' . $extension);
            $this->tempFiles[] = $exportFile;
        }
    }

    /**
    * @inheritdoc
    * Just update default escape mode
    */
    private function translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($sToTranslate, $sEscapeMode, $sLanguage);
    }

    /**
     * get available export type
     */
    private function getAvailableExportList()
    {
        Yii::app()->loadHelper("admin/exportresults");
        $resultsService = new ExportSurveyResultsService();
        $exports = $resultsService->getExports();
        $exports = array_filter($exports);
        $exportData = array();
        foreach ($exports as $key => $plugin) {
            $event = new PluginEvent('listExportOptions');
            $event->set('type', $key);
            $oPluginManager = App()->getPluginManager();
            $oPluginManager->dispatchEvent($event, $plugin);
            $exportData[$key] = $event->get('label');
        }
        return array_filter($exportData);
    }

    /**
     * Export response
     * @param int $surveyId Survey id
     * @param int $surveyId response id
     * @param string $exportType export type
     * @param string $headingFormat
     * @param string $answerFormat
     * @return string : the temporary filename
     */
    private function getExportFile($surveyId, $srid, $exportType, $headingFormat, $answerFormat)
    {
        $srid = intval($srid);
        $sFilter = "{{survey_{$surveyId}}}.id = {$srid}";

        /* language … */
        $language = App()->getLanguage();
        $survey = \Survey::model()->findByPk($surveyId);
        if (!in_array($language, $survey->getAllLanguages())) {
            $language = $survey->language;
        }
        $config = App()->getConfig('ResponseExportInEmail');
        /* columns : add an option */
        $aFields = array_keys(createFieldMap($survey, 'full', true, false, $language));

        Yii::app()->loadHelper('admin/exportresults');
        Yii::app()->loadHelper('export');
        Yii::import('application.helpers.viewHelper');
        //\viewHelper::disableHtmlLogging();
        $oExport = new \ExportSurveyResultsService();
        $exports = $oExport->getExports();
        $oFormattingOptions = new \FormattingOptions();
        $oFormattingOptions->responseMinRecord = 1;
        $oFormattingOptions->responseMaxRecord = SurveyDynamic::model($surveyId)->getMaxId();
        $oFormattingOptions->selectedColumns = $aFields;
        $oFormattingOptions->responseCompletionState = 'all';
        $oFormattingOptions->headingFormat = $headingFormat;
        $oFormattingOptions->answerFormat = $answerFormat;
        /* by config */
        $oFormattingOptions->convertY = $config['convertY'];
        $oFormattingOptions->yValue = $config['yValue'];
        $oFormattingOptions->convertN = $config['convertN'];
        $oFormattingOptions->nValue = $config['nValue'];
        $oFormattingOptions->csvMaskEquations = $config['csvMaskEquations'];
        $oFormattingOptions->useEMCode = $config['useEMCode'];
        $oFormattingOptions->headCodeTextSeparator = $config['headCodeTextSeparator'];
        $oFormattingOptions->csvFieldSeparator = $config['csvFieldSeparator'];
        $oFormattingOptions->stripHtmlCode = $config['stripHtmlCode'];
        $oFormattingOptions->headerSpacesToUnderscores = $config['headerSpacesToUnderscores'];
        $oFormattingOptions->headingTextLength = $config['headingTextLength'];
        /* Export as file */
        $oFormattingOptions->output = 'file';
        return $oExport->exportResponses($surveyId, $language, $exportType, $oFormattingOptions, $sFilter);
    }
}
