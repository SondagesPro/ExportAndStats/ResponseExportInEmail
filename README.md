# ResponseExportInEmail

Allow to attach export of current response.

## Documentation

Choose the export type in plugin settngs of survey setting, you can chose an export for confirmation, admin notification and admin detalied response email.

For each email type : you can choose the final filename, the heading format and the answer format.

For specific export type, if extension is invalid : you can choose to set extension using config variable in php.

You can set FormattingOptions using same config variable in config.php file, see config-default.php for available options.


For example, to set csv as tsv (Tab separated value) :
```
    'config'=>array(
		'debug'=>2,
		'debugsql'=>0,
		 // Update default LimeSurvey config here
		 'ResponseExportInEmail' => [
			'csvFieldSeparator' => "\t",
			'extensionByType' => [
				'csv' => 'tsv'
			]
		 ]
	)
```

## Support

If you need support on configuration and installation of this plugin : [create a support ticket on support.sondages.pro](https://support.sondages.pro/).

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab]( https://gitlab.com/SondagesPro/ExportAndStats/ResponseExportInEmail).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu <https://sondages.pro>
- Copyright © 2022 OECD <https://oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
