<?php

/**
 * This default config can be updated in application/config/config.php file
 * Inside config array 'ResponseExportInEmail' = []
 * Do not update this file !
 */

return array(
    /* Allow to set extension according to type */
    'extensionByType' => array(
        'csv' => 'csv',
        'doc' => 'doc'
    ),
    /**
     * Replace of FormattingOptions for export
     * @see \FormattingOptions (./application/helpers/admin/export/FormattingOptions.php) detail
     **/
    'convertY' => false,
    'yValue' => 1,
    'convertN' => false,
    'nValue' => 0,
    'csvMaskEquations' => true,
    'useEMCode' => true,
    'headCodeTextSeparator' => '. ',
    'csvFieldSeparator' => ',',
    'stripHtmlCode' => true,
    'headerSpacesToUnderscores' => false,
    'headingTextLength' => null,
);
